# RNM UNIVERSE PROJECT #

Build an Android project with the given public API below

### The project will consist of  ###

* Screen A will contain a list of characters and each character card containing the character's name and picture
* Screen B, which will be called by clicking on a card on screen A, will display further information about the selected character such as name, picture, status, species and a list or text containing the number of each episode (Not to be confused with the number of episodes in total) said character appears on

### Requirements ###

* Must be written in Kotlin language
* Must use the following libraries: Retrofit and Glide
* Must use the following Jetpack API components: navigation, cardview, view binding or data binding, livedata and viewmodel
* Should follow Material Design guidelines

### Nice-to-haves ###

* MVVM architecture pattern
* Database integration
* Unit testing

### API ###

* https://rickandmortyapi.com/api/character

### Docs ###

* https://rickandmortyapi.com/documentation